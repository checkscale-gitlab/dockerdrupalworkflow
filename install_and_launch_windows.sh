#!/bin/bash
set -eux
./download_drupal_windows.sh
./create_dev_volumes_windows.sh
docker-compose up -d
chmod a+w  "./drupal/web/sites/default/settings.php"
sleep 20
./install_drupal_windows.sh
echo "Enter to http://localhost/"
