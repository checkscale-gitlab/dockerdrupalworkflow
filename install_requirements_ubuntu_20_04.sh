#!/bin/bash
source .env
set -eux
# Git and curl
sudo apt install git curl
# Php CLI dependencies
sudo apt install php7.4-cli php7.4-gd php7.4-curl php7.4-mysql php7.4-zip php7.4-xml php7.4-mysqli php7.4-mbstring unzip
# Mysql Client
sudo apt install mysql-client
# NodeJS and Npm
sudo apt install nodejs npm
#Composer
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer --version
#Drush
composer global require drush/drush
echo "export PATH=$PATH:$HOME/.config/composer/vendor/bin" >> .dockerrc
source .dockerrc
drush --version
#Docker
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt install docker-ce
sudo groupadd -g $DGID drupal
sudo useradd -d /home/drupal -s /bin/bash -u $DUID -g $DGID drupal
sudo mkdir /home/drupal
sudo chown -R drupal:drupal /home/drupal
sudo cp .dockerrc /home/drupal/.bashrc
sudo usermod -aG docker ${USER}
sudo usermod -aG docker drupal
sudo usermod -aG drupal ${USER}
# Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
# Reboot
sudo reboot
