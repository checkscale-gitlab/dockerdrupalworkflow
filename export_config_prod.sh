#!/bin/bash
source .env
set -eux 
sudo chmod -R 777  ./volumes-$ENV_STAGE/app-tmp/import/
#import all configs
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  config-export --destination /tmp/import
sudo cp -r ./volumes-$ENV_STAGE/app-tmp/import/* ./drupal_etc/drupal_sync_all
for d in $(find ./drupal_etc/drupal_sync_all -maxdepth 2 -type d); do for file in $d/*; do if [ -e $file ]; then echo $file; fi; done; done;
