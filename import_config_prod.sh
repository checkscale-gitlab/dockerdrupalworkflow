#!/bin/bash
source .env
set -eux
sudo chmod 777 ./volumes-prod/app-tmp
sudo mkdir -p ./volumes-prod/app-tmp/import
#import search_api configs
#sudo cp ./drupal_etc/drupal_sync/search_api* ./volumes/app-tmp/import
#import gutenberg configs
#sudo cp ./drupal_etc/drupal_sync/gutenberg* ./volumes/app-tmp/import
#import all configs
sudo cp -r ./drupal_etc/drupal_sync_prod/* ./volumes/app-tmp/import
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  config-import --partial --source /tmp/import