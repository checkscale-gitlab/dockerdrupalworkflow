#!/bin/bash
source .env
set -eux
# Locale management
docker-compose exec -u www-data \
  app drush  -y \
  locale-check
docker-compose exec -u www-data \
  app drush  -y \
  locale-update
docker-compose exec -u www-data \
  app drush  -y \
  updatedb-status
# Drush Cron
docker-compose exec -u www-data \
  app drush  -y \
  cron
# Rebuild cache
docker-compose exec -u www-data \
  app drush  -y \
  cache-rebuild