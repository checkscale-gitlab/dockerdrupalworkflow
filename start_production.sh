#!/bin/bash
set -eux
sudo mkdir -p volumes-prod
sudo chmod 777 volumes-prod
sudo mkdir -p volumes-prod/mariadb-tmp
sudo chmod 777 volumes-prod/mariadb-tmp
sudo mkdir -p volumes-prod/app-tmp
sudo chmod 777 volumes-prod/app-tmp
sudo mkdir -p volumes-prod/logs
sudo chmod 777 volumes-prod/logs
sudo mkdir -p volumes-prod/solr-data
sudo chmod 777 volumes-prod/solr-data
docker-compose -f docker-compose-prod.yaml up -d
sleep 10
./install_production.sh
sudo chmod -R 777 drupal_etc/drupal_public/
sudo chmod -R 777 drupal_etc/drupal_private/
sudo mkdir -p ./volumes-prod/app-tmp/import
sudo chmod -R 777 ./volumes-prod/app-tmp/import
sudo cp -r ./drupal_etc/drupal_sync_prod/* ./volumes-prod/app-tmp/import
docker-compose -f docker-compose-prod.yaml \
  exec -u www-data \
  app-prod drush  -y \
  config-import --partial --source /tmp/import
# Locale management
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  locale-check
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  locale-update
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  updatedb-status
# Drush Cron
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  cron
# Rebuild cache
docker-compose -f docker-compose-prod.yaml exec -u www-data \
  app-prod drush  -y \
  cache-rebuild

