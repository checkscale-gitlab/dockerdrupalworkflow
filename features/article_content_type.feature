@d8 @api
Feature: Article content type
  Check Article content type

  Scenario Outline: Test that Article content type is created

    Given I am not logged in
    When I visit "/user"

    Then I should see "Username"
    When I enter "admin" for "Username"
    And I enter "admin" for "Password"
    And I press the "Log in" button

    Then I should see the link "View"
    And I should see the link "Edit"

    When I am on "/admin/structure/types"
    Then I should see the text <content_type>

    Examples:
      | content_type |
      | "Artículo"   |

  Scenario Outline: Test that Article content type fields items are created

    Given I am not logged in
    When I visit "/user"

    Then I should see "Username"
    When I enter "admin" for "Username"
    And I enter "admin" for "Password"
    And I press the "Log in" button

    Then I should see the link "View"
    And I should see the link "Edit"

    When I am on "/admin/structure/types/manage/article/fields"
    Then I should see the text <fields>

    Examples:
      | fields        |
      | "body"        |
      | "field_image" |
      | "comment"     |
      | "field_tags"  |