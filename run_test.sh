#!/bin/bash
source .env
set -eux

if [ $# -eq 0 ]
  then
    ./drupal/vendor/bin/behat
  else
    ./drupal/vendor/bin/behat $1
fi
