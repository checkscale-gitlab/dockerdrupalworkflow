#!/bin/bash
source .env
set -eux

docker-compose exec -u www-data app drush  -y locale-check
docker-compose exec -u www-data app drush  -y locale-update
docker-compose exec -u www-data app drush  -y updatedb-status

docker-compose exec -u www-data app drush  -y cron

docker-compose exec -u www-data app drush  -y cache-rebuild