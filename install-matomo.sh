#!/bin/bash
set -eux

# systemCheck
#curl http://localhost/index.php?action=systemCheck
#curl http://localhost/index.php?action=databaseSetup
rm -f config/config.ini.php
# databaseSetup
curl --data "host=matomo-mariadb&username=matomo&password=matomo&dbname=matomo&tables_prefix=matomo_&adapter=PDO%5CMYSQL" "http://localhost/index.php?action=databaseSetup&module=Installation&deleteTables=1"

# tablesCreation
curl "http://localhost/index.php?action=tablesCreation&module=Installation"
# setupSuperUser
curl --data "login=admin&password=admin123&password_bis=admin123&email=pepesan%40gmail.com" "http://localhost/index.php?action=setupSuperUser&module=Installation"
# firstWebsiteSetup
curl --data "siteName=pantera&url=http://localhost/&timezone=Europe/Madrid&ecommerce=0" "http://localhost/index.php?action=firstWebsiteSetup&module=Installation"
# finished
curl --data "do_not_track=1&anonymise_ip=1" "http://localhost/index.php?action=finished&module=Installation&deleteTables=1&site_idSite=1&site_name=pantera"
echo "[General]" >> config/config.ini.php
echo "trusted_hosts[] = 'localhost:8082'" >> config/config.ini.php
echo "trusted_hosts[] = 'matomo-app'" >> config/config.ini.php
#update core
#curl --data 'updateCorePlugins=1' http://localhost/index.php
# php /var/www/html/console core:update