#!/bin/bash
source .env
set -eux
sudo mkdir -p volumes-$ENV_STAGE
sudo chmod 777 volumes-$ENV_STAGE
sudo mkdir -p volumes-$ENV_STAGE/mariadb-tmp
sudo chmod 777 volumes-$ENV_STAGE/mariadb-tmp
sudo mkdir -p volumes-$ENV_STAGE/matomo-tmp
sudo chmod 777 volumes-$ENV_STAGE/matomo-tmp
sudo mkdir -p volumes-$ENV_STAGE/mariadb-matomo-tmp
sudo chmod 777 volumes-$ENV_STAGE/mariadb-matomo-tmp
sudo mkdir -p volumes-$ENV_STAGE/app-tmp
sudo chmod 777 volumes-$ENV_STAGE/app-tmp
sudo mkdir -p volumes-$ENV_STAGE/app-tmp/content_sync
sudo chmod 777 volumes-$ENV_STAGE/app-tmp/content_sync
sudo mkdir -p volumes-$ENV_STAGE/solr-data
sudo chmod 777 volumes-$ENV_STAGE/solr-data
sudo mkdir -p volumes-$ENV_STAGE/logs
sudo chmod 777 volumes-$ENV_STAGE/logs