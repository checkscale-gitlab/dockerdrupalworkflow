#!/bin/bash
source .env
set -eux
# Script to quickly create sub-theme on docker
sudo cp create_subtheme.sh ./drupal/web
sudo chmod +x ./drupal/web/create_subtheme.sh
docker-compose exec \
  -e CUSTOM_BARRIO=$CUSTOM_BARRIO \
  -e CUSTOM_BARRIO_NAME="$CUSTOM_BARRIO_NAME" \
  -e BASE_THEME_PATH=$BASE_THEME_PATH \
  app \
  ./create_subtheme.sh

sudo rm ./drupal/web/create_subtheme.sh
