#!/bin/bash
source .env

docker-compose exec app composer require drupal/$1 -d /var/www
docker-compose exec -u www-data app drush -y en $1