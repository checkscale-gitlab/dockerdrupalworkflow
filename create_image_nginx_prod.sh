#!/bin/bash
set -eux
source .env

echo "Creating Nginx Image:${DOCKER_IMAGE_NGINX_PROD}:${DOCKER_IMAGE_PROD_VERSION}"
docker build -t $DOCKER_IMAGE_NGINX_PROD:$DOCKER_IMAGE_PROD_VERSION -f ./Dockerfile.dev.nginx .
docker build -t $DOCKER_IMAGE_NGINX_PROD:latest -f ./Dockerfile.dev.nginx .
#    --no-cache
echo "Loging in"
docker login
echo "Uploading the image to the repository"
docker push $DOCKER_IMAGE_NGINX_PROD:$DOCKER_IMAGE_PROD_VERSION
docker push $DOCKER_IMAGE_NGINX_PROD:latest

